jQuery(document).ready(function($) { 	$('.toggle').each(function(){ 		$(this).find('.descs').hide(); 	});
		$('.toggle .trig').click(function(){		var el = $(this), parent = el.closest('.toggle');			if( el.hasClass('active') )
		{			parent.find('.descs').slideToggle();			el.removeClass('active');		}
		else		{			parent.find('.descs').slideToggle();			el.addClass('active');		}		return false;	});  }); 

// plsumus
$('.addpm').click(function () {
    $(this).prev().val(+$(this).prev().val() + 1);
});
$('.subpm').click(function () {
    if ($(this).next().val() > 0) $(this).next().val(+$(this).next().val() - 1);
});





    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
autoPlay : 3000,
      navigation : false,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true

      });
    });
	
	
// moving forms

$(".mov_frm2").hide();
$(".mov_frm3").hide();
$(".mov_frm4").hide();
$(".mov_frm1b").hide();
$(".mov_frm2b").hide();
$(".mov_frm1c").hide();
$(".mov_frm2c").hide();
$( ".btmovfrm1" ).click(function() {
$(".mov_frm1").hide();
$(".mov_frm2").fadeIn();
});

$( ".btmovfrm2" ).click(function() {
$(".mov_frm1").hide();
$(".mov_frm2").hide();
$(".mov_frm3").fadeIn();
});


$( ".btmovfrm3" ).click(function() {
$(".mov_frm1").hide();
$(".mov_frm2").hide();
$(".mov_frm3").hide();
$(".mov_frm4").fadeIn();
});



$( ".btmovfrm2b" ).click(function() {
$(".mov_frm2").hide();
$(".mov_frm3").hide();
$(".mov_frm4").hide();
$(".mov_frm1").fadeIn();
});

$( ".btmovfrm3b" ).click(function() {
$(".mov_frm1").hide();
$(".mov_frm3").hide();
$(".mov_frm4").hide();
$(".mov_frm2").fadeIn();
});


$( ".tb_dis" ).click(function() {
$(".mov_frm1").hide();
$(".mov_frm2").hide();
$(".mov_frm3").hide();
$(".mov_frm4").hide();
$(".mov_frm1c").hide();
$(".mov_frm2c").hide();
$(".mov_frm2b").hide();
$(".mov_frm1b").fadeIn();
$(this).addClass("actv");
$(".tb_mov").removeClass("actv");
$(".tb_sto").removeClass("actv");
});


$( ".btmovfrm1b" ).click(function() {
$(".mov_frm1b").hide();
$(".mov_frm2b").fadeIn();
});


$( ".btmovfrm1c" ).click(function() {
$(".mov_frm1c").hide();
$(".mov_frm2c").fadeIn();
});



$( ".tb_mov" ).click(function() {
$(".mov_frm1b").hide();
$(".mov_frm2b").hide();
$(".mov_frm1c").hide();
$(".mov_frm2c").hide();
$(".mov_frm2").hide();
$(".mov_frm3").hide();
$(".mov_frm4").hide();
$(".mov_frm1").fadeIn();
$(this).addClass("actv");
$(".tb_dis").removeClass("actv");
$(".tb_sto").removeClass("actv");
});


$( ".tb_sto" ).click(function() {
$(".mov_frm1b").hide();
$(".mov_frm2b").hide();
$(".mov_frm1").hide();
$(".mov_frm2").hide();
$(".mov_frm3").hide();
$(".mov_frm4").hide();
$(".mov_frm1c").fadeIn();
$(this).addClass("actv");
$(".tb_dis").removeClass("actv");
$(".tb_mov").removeClass("actv");
});


// text animation

var TxtRotate = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10)  || 2000;
  this.txt = '';
  this.tick();
  this.isDeleting = false;
};

TxtRotate.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

  var that = this;
  var delta = 300 - Math.random() * 100;

  if (this.isDeleting) { delta /= 2; }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function() {
    that.tick();
  }, delta);
};

window.onload = function() {
  var elements = document.getElementsByClassName('txt-rotate');
  for (var i=0; i<elements.length; i++) {
    var toRotate = elements[i].getAttribute('data-rotate');
    var period = elements[i].getAttribute('data-period');
    if (toRotate) {
      new TxtRotate(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
  document.body.appendChild(css);
};

